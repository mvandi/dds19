﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace DSS19
{
    public class CustomerRepository
    {
        #region ADO.NET

        private ConnectionProvider provider;

        /*
        public Persistance(string dbPath)
        {
            provider = new ConnectionProvider(dbPath);
        }
        */

        public void Load()
        {
            Trace.WriteLine("[CustomerRepository#Load] Persitance reading DB...");
            try
            {
                using (var connection = provider.GetConnection(DbName.Orders))
                {
                    connection.Open();
                    var cmd = connection.CreateCommand();
                    cmd.CommandText = "SELECT id, time, customer, quant FROM ordini LIMIT 100;";

                    using (var reader = cmd.ExecuteReader())
                    {
                        var quant = new List<int>();
                        while (reader.Read())
                        {
                            quant.Add(Convert.ToInt32(reader["quant"]));
                            Trace.WriteLine($"[CustomerRepository#Load] {reader["id"]} {reader["customer"]} {reader["time"]} {reader["quant"]}");
                        }
                        Trace.WriteLine($"[CustomerRepository#Load] Customers: [{string.Join(", ", quant)}]");
                        reader.Close();
                    }
                }
            }
            catch (Exception e)
            {
                Trace.WriteLine($"[CustomerRepository#Load] Error while searching customers: {e.Message}");
            }
            Trace.WriteLine("[CustomerRepository#Load] Persitance reading done.");
        }

        public void Find(string customerID)
        {
            Trace.WriteLine("[CustomerRepository#Find] Persitance reading DB...");

            try
            {
                using (var connection = provider.GetConnection(DbName.Orders))
                {
                    connection.Open();
                    var cmd = connection.CreateCommand();
                    cmd.CommandText = $"SELECT TOP 100 id, time, customer, quant FROM ordini WHERE customer='{customerID}';";

                    using (var reader = cmd.ExecuteReader())
                    {
                        var quant = new List<int>();
                        while (reader.Read())
                        {
                            quant.Add(Convert.ToInt32(reader["quant"]));
                            Trace.WriteLine($"[CustomerRepository#Find]: {reader["id"]} {reader["time"]} {reader["quant"]}");
                        }
                        Trace.WriteLine($"[CustomerRepository#Find] Orders for {customerID}: [{string.Join(", ", quant)}]");
                        reader.Close();
                    }
                }
            }
            catch (Exception e)
            {
                Trace.WriteLine($"[CustomerRepository#Find] Error while searching customer: {e.Message}");
            }
            Trace.WriteLine("[CustomerRepository#Find] Persitance reading done.");
        }

        public void Create(string customerID)
        {
            Trace.WriteLine("[CustomerRepository#Create] Persitance creating customer...");

            try
            {
                using (var connection = provider.GetConnection(DbName.Orders))
                {
                    connection.Open();
                    var cmd = connection.CreateCommand();
                    cmd.CommandText = $"INSERT INTO ordini (customer, time, quant) VALUES ('{customerID}', 0, 0);";

                    var result = cmd.ExecuteNonQuery();

                    if (result > 0)
                        Trace.WriteLine($"[CustomerRepository#Create] Inserted customer {customerID} in orders table.");
                }
            }
            catch (Exception e)
            {
                Trace.WriteLine($"[CustomerRepository#Create] Error while creating customer: {e.Message}");
            }
        }

        public void Delete(string customerID)
        {
            Trace.WriteLine("[CustomerRepository#Delete] Persitance deleting customer from DB...");

            try
            {
                using (var connection = provider.GetConnection(DbName.Orders))
                {
                    connection.Open();
                    var cmd = connection.CreateCommand();
                    cmd.CommandText = $"DELETE FROM ordini WHERE customer='{customerID}';";

                    var result = cmd.ExecuteNonQuery();

                    if (result > 0)
                        Trace.WriteLine($"[CustomerRepository#Delete] Removed customer {customerID} from orders table.");
                }
            }
            catch (Exception e)
            {
                Trace.WriteLine($"[CustomerRepository#Delete] Error while deleting customer: {e.Message}");
            }
        }

        public void Update(string customerID, string newCustomerID)
        {
            Trace.WriteLine("[CustomerRepository#Update] Persitance updating customer...");

            try
            {
                using (var connection = provider.GetConnection(DbName.Orders))
                {
                    connection.Open();
                    var cmd = connection.CreateCommand();
                    cmd.CommandText = $"UPDATE ordini SET customer='{newCustomerID}' WHERE customer='{customerID}';";

                    var result = cmd.ExecuteNonQuery();

                    if (result > 0)
                        Trace.WriteLine($"[CustomerRepository#Update] Updated customer {customerID} to newcust.");
                }
            }
            catch (Exception e)
            {
                Trace.WriteLine($"[CustomerRepository#Update] Error while deleting customer: {e.Message}");
            }
        }
        #endregion

        private string dbPath;

        private static readonly Random RandomState = new Random(550);

        public CustomerRepository(string dbPath)
        {
            this.dbPath = dbPath;
        }

        public bool Exists(string customer)
        {
            var ret = false;
            try
            {
                var orders = new List<string>();
                using (var ctx = CreateContext())
                {
                    var res = ctx.Database.SqlQuery<int>($"SELECT COUNT(*) FROM ordini WHERE customer='{customer}';").First();
                    ret = res > 0;
                }
                var str = ret ? "exists" : "does not exist";
                Trace.WriteLine($"[CustomerRepository#Exists] {customer} {str}.");
            }
            catch (Exception e)
            {
                Trace.WriteLine($"[CustomerRepository#Exists] Error: {e.Message}");
            }
            return ret;
        }

        public List<string> Get(int n, bool shuffle = true)
        {
            List<string> ret = null;
            try
            {
                using (var ctx = CreateContext())
                {
                    var orderBy = shuffle ? " ORDER BY RANDOM()" : "";
                    ret = ctx.Database.SqlQuery<string>($"SELECT distinct customer from ordini{orderBy} LIMIT {n}").ToList();
                }
                Trace.WriteLine($"[CustomerRepository#Get] {string.Join(", ", ret)}");
            }
            catch (Exception e)
            {
                Trace.WriteLine($"[CustomerRepository#Get] Error: {e.Message}");
            }
            return ret;
        }

        public List<string> All()
        {
            List<string> ret = null;
            try
            {
                using (var ctx = CreateContext())
                {
                    ret = ctx.Database.SqlQuery<string>("SELECT DISTINCT customer FROM ordini").ToList();
                }
                Trace.WriteLine($"[CustomerRepository#All] {string.Join(", ", ret)}");
            }
            catch (Exception e)
            {
                Trace.WriteLine($"[CustomerRepository#All] Error: {e.Message}");
            }
            return ret;
        }

        public string GetOrders(string customer)
        {
            var ret = "Error reading DB";
            try
            {
                var orders = new List<string>();
                using (var ctx = CreateContext())
                {
                    orders = ctx.Database.SqlQuery<DbResult>($"SELECT time, quant FROM ordini WHERE customer='{customer}' ORDER BY time")
                        .Select(r => $"{r.quant} - {r.time}")
                        .ToList();
                }
                ret = string.Join(", ", orders);
                Trace.WriteLine($"[CustomerRepository#GetOrders] {ret}");
            }
            catch (Exception e)
            {
                Trace.WriteLine($"[CustomerRepository#GetOrders] Error: {e.Message}");
            }
            return ret;
        }

        // Reads an instance from the db
        public GAP GetGAP()
        {
            var instance = new GAP();

            try
            {
                instance.zub = double.MaxValue;
                instance.zlb = double.MinValue;

                using (var ctx = CreateContext())
                {
                    List<int> capacity = ctx.Database.SqlQuery<int>("SELECT cap from capacita").ToList();
                    instance.m = capacity.Count();
                    instance.cap = new int[instance.m];
                    for (var i = 0; i < instance.m; i++)
                        instance.cap[i] = capacity[i];

                    List<double> costs = ctx.Database.SqlQuery<double>("SELECT cost from costi").ToList();
                    instance.n = costs.Count / instance.m;
                    instance.c = new double[instance.m, instance.n];
                    instance.req = new int[instance.n];
                    instance.sol = new int[instance.n];
                    instance.solbest = new int[instance.n];

                    for (var i = 0; i < instance.m; i++)
                        for (var j = 0; j < instance.n; j++)
                            instance.c[i, j] = costs[i * instance.n + j];

                    for (var j = 0; j < instance.n; j++)
                        instance.req[j] = -1;          // placeholder
                }
            }
            catch (Exception e)
            {
                Trace.WriteLine($"[CustomerRepository#GetGAP] Error: {e.Message}");
            }

            Trace.WriteLine("[CustomerRepository#GetGAP] Fine lettura dati istanza GAP");
            return instance;
        }

        public int GetOrderCount(string customer)
        {
            int ret = int.MinValue;
            try
            {
                using (var ctx = CreateContext())
                {
                    ret = ctx.Database.SqlQuery<int>($"SELECT COUNT(*) FROM ordini WHERE customer='{customer}'").First();

                }
                Trace.WriteLine($"[CustomerRepository#GetOrderCount] {ret}");
            }
            catch (Exception e)
            {
                Trace.WriteLine($"[CustomerRepository#GetOrderCount] Error: {e.Message}");
            }
            return ret;
        }
        
        private SQLiteDatabaseContext CreateContext()
        {
            return new SQLiteDatabaseContext(dbPath);
        }
    }


    class DbResult
    {
        public int time { get; set; }
        public int quant { get; set; }
    }
}
