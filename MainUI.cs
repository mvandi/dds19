﻿using System;
using System.Windows.Forms;
using System.Diagnostics;

namespace DSS19
{
    public partial class MainUI : Form
    {
        private Controller controller;

        public MainUI()
        {
            InitializeComponent();

            var textBoxListener = new TextBoxTraceListener(txtConsole);
            Trace.Listeners.Add(textBoxListener);
            
            var dbPath = ChooseFile("Select database file");
            var scriptsPath = ChooseFolder("Select Python scripts folder");
            var pythonPath = ChooseFile("Select Python interpreter");

            var runner = new PythonRunner(pythonPath, 20000);

            var repository = new CustomerRepository(dbPath);
            var model = new Forecaster(runner, dbPath, scriptsPath);
            controller = new Controller(repository, model);
        }

        private string ChooseFile(string title)
        {
            var dfd = new OpenFileDialog();
            dfd.Title = title;

            var ret = "";

            if (dfd.ShowDialog() == DialogResult.OK)
            {
                ret = dfd.FileName;
            }
            return ret;
        }

        private string ChooseFolder(string title)
        {
            var fbd = new FolderBrowserDialog();

            fbd.Description = title;

            var ret = "";

            if (fbd.ShowDialog() == DialogResult.OK)
            {
                ret = fbd.SelectedPath;
            }
            return ret;
        }

        private void btnCustomersChart_Click(object sender, EventArgs e)
        {
            ReadDB();
        }

        private void itmCustomersChart_Click(object sender, EventArgs e)
        {
            ReadDB();
        }

        private void ReadDB()
        {
            OrdersChart();
        }

        private async void OrdersChart()
        {
            EnableAll(false);

            controller.Load();
            picChart.Image = await controller.OrdersChart();

            EnableAll(true);
        }

        private void btnChart_Click(object sender, EventArgs e)
        {
            ForecastChart();
        }

        private void itmChart_Click(object sender, EventArgs e)
        {
            ForecastChart();
        }

        private async void ForecastChart()
        {
            EnableAll(false);

            picChart.Image = await controller.ForecastChart(Customer);

            EnableAll(true);
        }

        private void btnSARIMA_Click(object sender, EventArgs e)
        {
            ForecastSARIMA();
        }

        private void itmSARIMA_Click(object sender, EventArgs e)
        {
            ForecastSARIMA();
        }

        private async void ForecastSARIMA()
        {
            EnableAll(false);

            var res = await controller.ForecastRequests(Customer);
            Trace.WriteLine($"Requests forecast for {res.Customer} is {res.Value}.");

            EnableAll(true);
        }

        private void btnGAP_Click(object sender, EventArgs e)
        {
            LocalSearch();
        }

        private void itmGAP_Click(object sender, EventArgs e)
        {
            LocalSearch();
        }

        private async void LocalSearch()
        {
            EnableAll(false);

            await controller.OptimizeGAP();

            EnableAll(true);
        }

        private string Customer
        {
            get { return txtCustomer.Text; }
        }

        private void EnableAll(bool enabled)
        {
            itmCustomersChart.Enabled = enabled;
            itmChart.Enabled = enabled;
            itmSARIMA.Enabled = enabled;
            itmOptimizeGAP.Enabled = enabled;

            txtCustomer.Enabled = enabled;

            btnCustomersChart.Enabled = enabled;
            btnChart.Enabled = enabled;
            btnSARIMA.Enabled = enabled;
            btnOptimizeGAP.Enabled = enabled;
        }
/*
        private void SearchCustomer()
        {
            controller.Search(CustomerID);
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            CreateCustomer();
        }

        private void CreateCustomer()
        {
            controller.Create(NewCustomerID);
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            DeleteCustomer();
        }

        private void DeleteCustomer()
        {
            controller.Delete(CustomerID);
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            UpdateCustomer();
        }

        private void UpdateCustomer()
        {
            controller.Update(CustomerID, NewCustomerID);
        }

        private string NewCustomerID
        {
            get { return txtNewCustomer.Text; }
        }
*/
    }

    public class TextBoxTraceListener : TraceListener
    {
        private TextBox target;
        private StringSendDelegate invokeWrite;

        public TextBoxTraceListener(TextBox target)
        {
            this.target = target;
            invokeWrite = new StringSendDelegate(SendString);
        }

        public override void Write(string message)
        {
            target.Invoke(invokeWrite, new object[] { message });
        }

        public override void WriteLine(string message)
        {
            target.Invoke(invokeWrite, new object[] { message + Environment.NewLine });
        }

        private delegate void StringSendDelegate(string message);

        private void SendString(string message)
        {
            target.AppendText(message);
        }
    }
}
