﻿namespace DSS19
{
    partial class MainUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainUI));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnCustomersChart = new System.Windows.Forms.ToolStripButton();
            this.btnChart = new System.Windows.Forms.ToolStripButton();
            this.btnSARIMA = new System.Windows.Forms.ToolStripButton();
            this.btnOptimizeGAP = new System.Windows.Forms.ToolStripButton();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.itmCustomersChart = new System.Windows.Forms.ToolStripMenuItem();
            this.forecastToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.itmChart = new System.Windows.Forms.ToolStripMenuItem();
            this.itmSARIMA = new System.Windows.Forms.ToolStripMenuItem();
            this.localSearchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.itmOptimizeGAP = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.txtCustomer = new System.Windows.Forms.TextBox();
            this.lblCustomer = new System.Windows.Forms.Label();
            this.txtConsole = new System.Windows.Forms.TextBox();
            this.picChart = new System.Windows.Forms.PictureBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picChart)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(28, 28);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnCustomersChart,
            this.btnChart,
            this.btnSARIMA,
            this.btnOptimizeGAP});
            this.toolStrip1.Location = new System.Drawing.Point(0, 38);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(800, 35);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btnCustomersChart
            // 
            this.btnCustomersChart.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCustomersChart.Image = ((System.Drawing.Image)(resources.GetObject("btnCustomersChart.Image")));
            this.btnCustomersChart.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCustomersChart.Name = "btnCustomersChart";
            this.btnCustomersChart.Size = new System.Drawing.Size(32, 32);
            this.btnCustomersChart.Text = "Draw customers chart";
            this.btnCustomersChart.ToolTipText = "Testo di aiuto";
            this.btnCustomersChart.Click += new System.EventHandler(this.btnCustomersChart_Click);
            // 
            // btnChart
            // 
            this.btnChart.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnChart.Image = ((System.Drawing.Image)(resources.GetObject("btnChart.Image")));
            this.btnChart.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnChart.Name = "btnChart";
            this.btnChart.Size = new System.Drawing.Size(32, 32);
            this.btnChart.Text = "Draw customer chart";
            this.btnChart.Click += new System.EventHandler(this.btnChart_Click);
            // 
            // btnSARIMA
            // 
            this.btnSARIMA.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSARIMA.Image = ((System.Drawing.Image)(resources.GetObject("btnSARIMA.Image")));
            this.btnSARIMA.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSARIMA.Name = "btnSARIMA";
            this.btnSARIMA.Size = new System.Drawing.Size(32, 32);
            this.btnSARIMA.Text = "Predict customer requests";
            this.btnSARIMA.Click += new System.EventHandler(this.btnSARIMA_Click);
            // 
            // btnOptimizeGAP
            // 
            this.btnOptimizeGAP.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnOptimizeGAP.Image = ((System.Drawing.Image)(resources.GetObject("btnOptimizeGAP.Image")));
            this.btnOptimizeGAP.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnOptimizeGAP.Name = "btnOptimizeGAP";
            this.btnOptimizeGAP.Size = new System.Drawing.Size(32, 32);
            this.btnOptimizeGAP.Text = "Optimize GAP instance";
            this.btnOptimizeGAP.Click += new System.EventHandler(this.btnGAP_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(28, 28);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.forecastToolStripMenuItem,
            this.localSearchToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 38);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.itmCustomersChart});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(56, 34);
            this.toolStripMenuItem1.Text = "File";
            // 
            // itmCustomersChart
            // 
            this.itmCustomersChart.Name = "itmCustomersChart";
            this.itmCustomersChart.Size = new System.Drawing.Size(240, 34);
            this.itmCustomersChart.Text = "Read database";
            this.itmCustomersChart.Click += new System.EventHandler(this.itmCustomersChart_Click);
            // 
            // forecastToolStripMenuItem
            // 
            this.forecastToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.itmChart,
            this.itmSARIMA});
            this.forecastToolStripMenuItem.Name = "forecastToolStripMenuItem";
            this.forecastToolStripMenuItem.Size = new System.Drawing.Size(102, 34);
            this.forecastToolStripMenuItem.Text = "Forecast";
            // 
            // itmChart
            // 
            this.itmChart.Name = "itmChart";
            this.itmChart.Size = new System.Drawing.Size(181, 34);
            this.itmChart.Text = "Chart";
            this.itmChart.Click += new System.EventHandler(this.itmChart_Click);
            // 
            // itmSARIMA
            // 
            this.itmSARIMA.Name = "itmSARIMA";
            this.itmSARIMA.Size = new System.Drawing.Size(181, 34);
            this.itmSARIMA.Text = "SARIMA";
            this.itmSARIMA.Click += new System.EventHandler(this.itmSARIMA_Click);
            // 
            // localSearchToolStripMenuItem
            // 
            this.localSearchToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.itmOptimizeGAP});
            this.localSearchToolStripMenuItem.Name = "localSearchToolStripMenuItem";
            this.localSearchToolStripMenuItem.Size = new System.Drawing.Size(109, 34);
            this.localSearchToolStripMenuItem.Text = "Optimize";
            // 
            // itmOptimizeGAP
            // 
            this.itmOptimizeGAP.Name = "itmOptimizeGAP";
            this.itmOptimizeGAP.Size = new System.Drawing.Size(288, 34);
            this.itmOptimizeGAP.Text = "GAP";
            this.itmOptimizeGAP.Click += new System.EventHandler(this.itmGAP_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 73);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.txtCustomer);
            this.splitContainer1.Panel1.Controls.Add(this.lblCustomer);
            this.splitContainer1.Panel1.Controls.Add(this.txtConsole);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.picChart);
            this.splitContainer1.Size = new System.Drawing.Size(800, 377);
            this.splitContainer1.SplitterDistance = 266;
            this.splitContainer1.TabIndex = 2;
            // 
            // txtCustomer
            // 
            this.txtCustomer.Location = new System.Drawing.Point(151, 11);
            this.txtCustomer.Name = "txtCustomer";
            this.txtCustomer.Size = new System.Drawing.Size(112, 29);
            this.txtCustomer.TabIndex = 2;
            // 
            // lblCustomer
            // 
            this.lblCustomer.AutoSize = true;
            this.lblCustomer.Location = new System.Drawing.Point(12, 11);
            this.lblCustomer.Name = "lblCustomer";
            this.lblCustomer.Size = new System.Drawing.Size(97, 25);
            this.lblCustomer.TabIndex = 1;
            this.lblCustomer.Text = "Customer";
            // 
            // txtConsole
            // 
            this.txtConsole.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtConsole.Location = new System.Drawing.Point(0, 61);
            this.txtConsole.Multiline = true;
            this.txtConsole.Name = "txtConsole";
            this.txtConsole.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtConsole.Size = new System.Drawing.Size(263, 313);
            this.txtConsole.TabIndex = 0;
            this.txtConsole.WordWrap = false;
            // 
            // picChart
            // 
            this.picChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picChart.Location = new System.Drawing.Point(0, 0);
            this.picChart.Name = "picChart";
            this.picChart.Size = new System.Drawing.Size(530, 377);
            this.picChart.TabIndex = 0;
            this.picChart.TabStop = false;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(28, 28);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.ImageScalingSize = new System.Drawing.Size(28, 28);
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(61, 4);
            // 
            // MainUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainUI";
            this.Text = "Form1";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picChart)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btnCustomersChart;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TextBox txtConsole;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.Label lblCustomer;
        private System.Windows.Forms.PictureBox picChart;

        private System.Windows.Forms.ToolStripButton btnChart;
        private System.Windows.Forms.ToolStripButton btnSARIMA;
        private System.Windows.Forms.ToolStripButton btnOptimizeGAP;

        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem forecastToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem localSearchToolStripMenuItem;

        private System.Windows.Forms.ToolStripMenuItem itmCustomersChart;
        private System.Windows.Forms.ToolStripMenuItem itmChart;
        private System.Windows.Forms.ToolStripMenuItem itmSARIMA;
        private System.Windows.Forms.ToolStripMenuItem itmOptimizeGAP;
        private System.Windows.Forms.TextBox txtCustomer;
    }
}
