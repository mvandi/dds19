﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace DSS19
{
    public enum DbName
    {
        Orders
    }

    public enum DbEngine
    {
        Default,
        SQLite,
        SQLServer
    }

    public class ConnectionProvider
    {
        private Dictionary<DbName, Dictionary<DbEngine, string>> connectionStrings;

        public ConnectionProvider(string dbPath)
        {
            connectionStrings = new Dictionary<DbName, Dictionary<DbEngine, string>>()
            {
                [DbName.Orders] = new Dictionary<DbEngine, string>()
                {
                    [DbEngine.SQLite] = GetConnectionString("SQLite").Replace("DBPATH", dbPath),
                    [DbEngine.SQLServer] = GetConnectionString("SQLServer")
                }
            };
        }

        public IDbConnection GetConnection(DbName database, DbEngine engine = DbEngine.Default)
        {
            if (engine == DbEngine.Default)
                engine = DefaultEngine;

            var factory = GetFactory(engine);

            var conn = factory.CreateConnection();
            conn.ConnectionString = ConnectionString(database, engine);
            return conn;
        }

        private static string GetConnectionString(string key)
        {
            return ConfigurationManager.ConnectionStrings[key].ConnectionString;
        }

        private string ConnectionString(DbName database, DbEngine engine)
        {
            return connectionStrings[database][engine];
        }

        private static DbEngine defaultEngine = DbEngine.Default;
        private static DbEngine DefaultEngine
        {
            get
            {
                if (defaultEngine == DbEngine.Default)
                    Enum.TryParse(ConfigurationManager.AppSettings["DefaultEngine"], out defaultEngine);
                return defaultEngine;
            }
        }

        private static DbProviderFactory GetFactory(DbEngine engine)
        {
            var key = engine.ToString();
            var providerName = ConfigurationManager.ConnectionStrings[key].ProviderName;
            return DbProviderFactories.GetFactory(providerName);
        }
    }
}
