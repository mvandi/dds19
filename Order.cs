﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DSS19
{
	[Table("ordini")]
	public class Order
	{
		//[Key, Column("PK_UID", TypeName = "INTEGER")]
		//public long pk_uid { get; set; }
        [Column("id", TypeName = "INTEGER")]
        public long Id { get; set; }
        [Column("customer", TypeName = "VARCHAR")]
        public string Customer { get; set; }
        [Column("time", TypeName = "INTEGER")]
        public int Time { get; set; }
        [Column("quant", TypeName = "INTEGER")]
        public int Quant { get; set; }
   }
}
