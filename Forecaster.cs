﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace DSS19
{
    public class Forecaster
    {
        private PythonRunner runner;

        private string dbPath;
        private string scriptsPath;

        public Forecaster(PythonRunner runner, string dbPath, string scriptsPath)
        {
            this.runner = runner;
            this.dbPath = dbPath;
            this.scriptsPath = scriptsPath;
        }

        public async Task<Bitmap> OrdersChart(List<string> customers)
        {
            var s = string.Join(",", customers.Select(c => SurroundWith(c, "'")));
            return await GetChart("chartOrders.py", s);
        }

        public async Task<Bitmap> ForecastChart(string customer)
        {
            customer = SurroundWith(customer, "'");
            return await GetChart("arima_forecast.py", customer);
        }

        public async Task<double> ForecastValue(string customer)
        {
            customer = SurroundWith(customer, "'");
            double ret = double.NaN;
            try
            {
                var output = await runner.getStringsAsync(
                        scriptsPath,
                        "arima_forecast.py",
                        scriptsPath,
                        dbPath,
                        customer);

                ret = output
                    .Split(new[] { Environment.NewLine }, StringSplitOptions.None)
                    .Where(s => s.StartsWith("Actual "))
                    .Select(s => s.Substring(s.LastIndexOf(' ')))
                    .Select(s => s.Trim())
                    .Select(s => Convert.ToDouble(s, CultureInfo.InvariantCulture))
                    .Last();
            }
            catch (Exception e)
            {
                Trace.WriteLine($"[Forecaster#ForecastValue] Error: {e.Message}");
            }
            return ret;
        }

        private async Task<Bitmap> GetChart(string script, string customers)
        {
            Bitmap ret = null;
            try
            {
                ret = await runner.getImageAsync(
                        scriptsPath,
                        script,
                        scriptsPath,
                        dbPath,
                        customers);
            }
            catch (Exception e)
            {
                Trace.WriteLine($"[Forecaster#GetChart] Error: {e.Message}");
            }
            return ret;
        }

        private static string SurroundWith(string s, string symbol)
        {
            if (string.IsNullOrEmpty(s))
                return s;
            if (!s.StartsWith(symbol))
                s = symbol + s;
            if (!s.EndsWith(symbol))
                s += symbol;
            return s;
        }
    }
}
