﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Threading.Tasks;
using System.Linq;
using System.IO;
using System.Collections.Generic;

namespace DSS19
{
    public class Controller
    {
        private CustomerRepository repository;
        private Forecaster model;

        private List<string> customers = null;

        public Controller(CustomerRepository repository, Forecaster model)
        {
            this.repository = repository;
            this.model = model;
        }

        public List<string> Load(int n = 12)
        {
            customers = repository.Get(n);
            return customers;
        }
        
        public async Task<Bitmap> OrdersChart()
        {
            return await model.OrdersChart(customers);
        }

        public async Task<Bitmap> ForecastChart(string customer)
        {
            customer = GetCustomerOrRandom(customer);
            return await model.ForecastChart(customer);
        }

        public async Task<ForecastResult> ForecastRequests(string customer)
        {
            customer = GetCustomerOrRandom(customer);
            var value = await model.ForecastValue(customer);

            return new ForecastResult(customer, value);
        }

        public async Task<int[]> ForecastAllRequests()
        {
            var customers = repository.All();

            var forecasts = new List<int>();

            foreach (var customer in customers)
            {
                Trace.WriteLine($"[Controller#ForecastAllRequests] Forecasting requests for '{customer}'...");

                var forecast = await model.ForecastValue(customer);
                forecast = Math.Round(forecast);
                forecasts.Add((int)forecast);

                Trace.WriteLine($"[Controller#ForecastAllRequests] Requests forecast for '{customer}' is {(int)forecast}");
            }

            return forecasts.ToArray();
        }

        public async Task<double> OptimizeGAP()
        {
            var instance = repository.GetGAP();
            instance.req = await ReadGapRequests("GAPreq.dat");

            var zub = instance.SimpleContruct();
            Trace.WriteLine($"[Controller#OptimizeGAP] simple contruct cost: {zub}");

            zub = instance.Opt10(instance.c);
            Trace.WriteLine($"[Controller#OptimizeGAP] Opt 1-0 cost: {zub}");

            zub = instance.TabuSearch(30, 100);
            Trace.WriteLine($"[Controller#OptimizeGAP] Tabu Search cost: {zub}");

            return zub;
        }

        private async Task<int[]> ReadGapRequests(string requestsFile)
        {
            if (File.Exists(requestsFile))
            {
                var txtData = File.ReadAllLines(requestsFile);
                return Array.ConvertAll(txtData, new Converter<string, int>(i => int.Parse(i)));
            }

            var requests = await ForecastAllRequests();
            Trace.WriteLine($"[Controller#ReadGapRequests] Forecasts for all customers ({requests.Count()}).");
            File.WriteAllLines(requestsFile, requests.Select(x => x.ToString()));

            return requests;
        }

        public void Search(string customer)
        {
            /*
            if (customer != "")
                persistance.Search(customer);
            else
                persistance.Load();
            */
            repository.GetOrderCount(customer);
        }

        public void Create(string customer)
        {
            if (customer != "")
                repository.Create(customer);
        }

        public void Delete(string customer)
        {
            if (customer != "")
                repository.Delete(customer);
        }

        public void Update(string customer, string newCustomer)
        {
            if (customer != "" && newCustomer != "")
                repository.Update(customer, newCustomer);
        }

        private string GetCustomerOrRandom(string customer)
        {
            if (string.IsNullOrEmpty(customer) || !repository.Exists(customer))
                customer = string.Join(", ", repository.Get(1));
            return customer;
        }
    }

    public class ForecastResult
    {
        public readonly string Customer;

        public readonly double Value;

        internal ForecastResult(string customer, double value)
        {
            Customer = customer;
            Value = value;
        }
    }
}
