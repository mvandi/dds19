﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace DSS19
{
    public class GAP
    {
        public int n;       // numero clienti
        public int m;       // numero magazzini
        public double[,] c; // costi d'assegnamento
        public int[] req;   // richieste clienti
        public int[] cap;   // capacità magazzini

        public int[] sol, solbest;    // per ogni cliente, il suo magazzino
        public double zub, zlb;

        const double EPS = 0.0001;
        Random rnd = new Random(550);

        public GAP()
        {
            zub = double.MaxValue;
            zlb = double.MinValue;
        }

        public double SimpleContruct()
        {
            int[] capleft = new int[cap.Length], indices = new int[m];
            var dist = new double[m];
            Array.Copy(cap, capleft, cap.Length);

            zub = 0;
            for (var j = 0; j < n; j++)
            {
                for (var i = 0; i < m; i++)
                {
                    dist[i] = c[i, j];
                    indices[i] = i;
                }

                Array.Sort(dist, indices);

                var ii = 0;
                while (ii < m)
                {
                    var i = indices[ii];
                    if (capleft[i] >= req[j])
                    {
                        sol[j] = i;
                        capleft[i] -= req[j];
                        zub += c[i, j];
                        break;
                    }
                    ii++;
                }
                if (ii == m)
                    Trace.WriteLine("[GAP#SimpleContruct]: ahi ahi.");
            }
            return zub;
        }

        public double Opt10(double[,] c)
        {
            double z = 0.0;
            var capres = new int[cap.Length];
            Array.Copy(cap, capres, cap.Length);

            for (var j = 0; j < n; j++)
            {
                capres[sol[j]] -= req[j];
                z += c[sol[j], j];
            }

        l0: for (var j = 0; j < n; j++)
            {
                var isol = sol[j];
                for (var i = 0; i < m; i++)
                {
                    if (i != isol && c[i, j] < c[isol, j] && capres[i] >= req[j])
                    {
                        sol[j] = i;
                        capres[i] -= req[j];
                        capres[isol] += req[j];
                        z -= c[isol, j] - c[i, j];
                        if (z < zub) zub = z;
                        goto l0;
                    }
                }
            }
            return z;
        }

        public double TabuSearch(int tenure, int numIterations)
        {
            /*
            1. Generate an initial feasible solution S, 
	           set S* = S and initialize TL=nil.
            2. Find S' \in N(S), such that 
	           z(S')=min {z(S^), \forall S \in N(S), S \notin TL}.
            3. S=S', TL=TL \in {S}, if (z(S*) > z(S)) set S* = S.
            4. If not(end condition) go to step 2.
            */
            var capres = new int[cap.Length];
            Array.Copy(cap, capres, cap.Length);
            for (var j = 0; j < n; j++)
                capres[sol[j]] -= req[j];

            var z = zub;
            Array.Copy(sol, solbest, sol.Length);

            var TL = new int[m, n];
            for (var i = 0; i < m; i++)
                for (var j = 0; j < n; j++)
                    TL[i, j] = -1;

            Trace.WriteLine("[GAP#TabuSearch] Tabu Search started.");
            for (var t = 0; t < numIterations; t++)
            {
                int isol, ibest, jbest;
                ibest = jbest = int.MinValue;
                var DeltaMax = double.NegativeInfinity;

                for (var j = 0; j < n; j++)
                {
                    isol = sol[j];
                    for (var i = 0; i < m; i++)
                    {
                        if (i == isol) continue;

                        // cerco di trovare la soluzione ammissibile che si allontana di piu' dalla soluzione precedente,
                        // non e' detto che sia la soluzione ottima.
                        double delta;
                        if (capres[i] >= req[j] && (TL[i, j] < 0 || t > (TL[i, j] + tenure)) && (delta = c[isol, j] - c[i, j]) > DeltaMax)
                        {
                            ibest = i;
                            jbest = j;
                            DeltaMax = delta;
                        }
                    }
                }

                // Aggiorna la capacita' residua degli assegnamenti migliori e calcola il costo della soluzione.
                isol = sol[jbest];
                sol[jbest] = ibest;
                capres[ibest] -= req[jbest];
                capres[isol] += req[jbest];

                z -= DeltaMax;

                if (z < zub)
                {
                    zub = z;
                    Array.Copy(sol, solbest, sol.Length);
                }

                TL[ibest, jbest] = t;

                Trace.WriteLine($"[GAP#TabuSearch] z: {z} - iteration: {t} - DeltaMax: {DeltaMax}");
            }

            Trace.WriteLine("[GAP#TabuSearch] Tabu Search ended.");
            Trace.WriteLine($"[GAP#TabuSearch] zub: {zub} - sol: ({string.Join(", ", solbest)})");

            return zub;
        }
    }
}
