﻿'''
Common functions and a colormap for the line charts.
'''

DEBUG=False

def debug_print(x):
	if DEBUG:
		print(x)

# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

from sqlalchemy import create_engine
import pandas as pd

def load_stock_data(db, tickers, start_date, end_date):
	'''
	Loads the stock data for the specified ticker symbols, and for the specified date range.
	:param db: Full path to database with stock data.
	:param tickers: A list with ticker symbols.
	:param start_date: The start date.
	:param end_date: The start date.
	:return: A list of time-indexed dataframe, one for each ticker, ordered by date.
	'''

	SQL = f"SELECT * FROM Quotes WHERE TICKER IN ({tickers}) AND Date >= '{start_date}' AND Date <= '{end_date}'"

	engine = create_engine('sqlite:///' + db)

	df_all = pd.read_sql(SQL, engine, index_col='Date', parse_dates='Date')
	df_all = df_all.round(2)

	result = []

	for ticker in tickers.split(','):
		df_ticker = df_all.query(f'Ticker == {ticker}')
		result.append(df_ticker)

	return result

def load_orders(db, customers):
	SQL = f'SELECT * FROM ordini WHERE customer IN ({customers})'

	engine = create_engine('sqlite:///' + db)

	df_allorders = pd.read_sql(SQL, engine, index_col='id')

	result = []

	for cust in customers.split(','):
		df_order = df_allorders.query(f'customer == {cust}')
		result.append(df_order)

	return result

# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

import io, sys, base64
import matplotlib.pyplot as plt

def print_figure(fig):
	'''
	Converts a figure (as created e.g. with matplotlib or seaborn) to a png image and this 
	png subsequently to a base64-string, then prints the resulting string to the console.
	'''
	
	if DEBUG:
		plt.show(fig)
	else:
		buf = io.BytesIO()
		fig.savefig(buf, format='png')
		print(base64.b64encode(buf.getbuffer()))

# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

# Custom colormap that is used with line charts
COLOR_MAP = [
	'blue', 'orange', 'green', 'red', 'purple', 'brown', 'pink', 'gray', 'olive', 'cyan',
	'darkblue', 'darkorange', 'darkgreen', 'darkred', 'rebeccapurple', 'darkslategray', 
	'mediumvioletred', 'dimgray', 'seagreen', 'darkcyan', 'deepskyblue', 'yellow', 
	'lightgreen', 'lightcoral', 'plum', 'lightslategrey', 'lightpink', 'lightgray', 
	'lime', 'cadetblue'
	]

# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

import sys
import os

def set_local_dir():
	if len(sys.argv) > 1:
		os.chdir(sys.argv[1])
	
	debug_print(f'local_dir: {os.getcwd()}')

def get_db_path(default_path):
	if len(sys.argv) < 3:
		db_path = default_path
	else:
		db_path = sys.argv[2]

	debug_print(f'DB path: {db_path}')
	return db_path

def get_customers(default_customers):
	if len(sys.argv) >= 4:
		customers = sys.argv[3]
	else:
		customers = default_customers

	debug_print(f'Customers: {customers}')
	return customers
