# python check_req.py -g ..\bin\GAPreq.dat -t ..\res\GAPreq.dat

import os
import argparse

def read_gapreq(path):
    with open(path, 'r') as f:
        return [int(line.strip()) for line in f]

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-t', '--target', required=True, type=str, help='Path to the target GAP requests file')
    args = parser.parse_args()

    generated = read_gapreq(args.generated)
    target = read_gapreq(args.target)

    print(f'generated: {generated}')
    print(f'target:    {target}')

    answer = 'Yes' if generated == target else 'No'
    print(f'Do files match? {answer}.')
