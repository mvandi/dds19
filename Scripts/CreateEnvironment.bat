@echo off

set ENVNAME=%1

call conda create -n %ENVNAME% python=3.6 -y

call conda activate %ENVNAME%

call conda install -n %ENVNAME% numpy scipy pandas statsmodels scikit-learn tensorflow tensorflow-gpu keras -y
call conda install -n %ENVNAME% -c conda-forge matplotlib sqlalchemy pylint cycler -y

call pip install pyramid-arima

call conda deactivate

set ENVNAME=
